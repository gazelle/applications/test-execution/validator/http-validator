---
title: Release note
subtitle: HTTP Validator
toolversion: 0.3.0
releasedate: 2023-08-28
author: Clément LAGORCE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-HTTP_VALIDATOR
---

# 0.3.0
_Release date: 2023-08-28_

__Story__
* \[[HTTP-16](https://gazelle.ihe.net/jira/browse/HTTP-16)\] Release/Doc
* \[[HTTP-43](https://gazelle.ihe.net/jira/browse/HTTP-43)\] Use new Validation API and Reports

__Improvement__
* \[[HTTP-40](https://gazelle.ihe.net/jira/browse/HTTP-40)\] CRLF Improvements

# 0.2.0
_Release date: 2023-02-22_

__Story__
* \[[HTTP-10](https://gazelle.ihe.net/jira/browse/HTTP-10)\] Support HTTP Header
* \[[HTTP-11](https://gazelle.ihe.net/jira/browse/HTTP-11)\] Selector using EL expression
* \[[HTTP-12](https://gazelle.ihe.net/jira/browse/HTTP-12)\] Support list of checker
* \[[HTTP-13](https://gazelle.ihe.net/jira/browse/HTTP-13)\] Support Conditionality
* \[[HTTP-14](https://gazelle.ihe.net/jira/browse/HTTP-14)\] Add needed Checkers
* \[[HTTP-19](https://gazelle.ihe.net/jira/browse/HTTP-19)\] Security integration (gazelle-token-service)

# 0.1.0
_Release date: 2023-01-30_

__Story__
* \[[HTTP-3](https://gazelle.ihe.net/jira/browse/HTTP-3)\] Import a validation profile via API
* \[[HTTP-4](https://gazelle.ihe.net/jira/browse/HTTP-4)\] Create model for validation profile (Minimal, V0.1)
* \[[HTTP-5](https://gazelle.ihe.net/jira/browse/HTTP-5)\] Project creation
* \[[HTTP-6](https://gazelle.ihe.net/jira/browse/HTTP-6)\] Engine (minimal, V0.1)
* \[[HTTP-7](https://gazelle.ihe.net/jira/browse/HTTP-7)\] Create Webservice
* \[[HTTP-8](https://gazelle.ihe.net/jira/browse/HTTP-8)\] Integrate into EVSClient