# HTTP-Validator

## HTTP Validator Scope

### Introduction
The aim of this tool is to permit the validation of HTTP messages (requests or responses) by using a JSON Validation Profile.
It can be called by other tools (e.g. EVS Client) through RESTful APIs and return a Valdiation result.

### Perimeter of the application
HTTP Validator's features are :
* Receive a validation request and respond with a validation report.
* List the Validation Profiles.
* Export a Validation Profile.
* Import a Validation Profile via :
  * The Graphical User Interface.
  * A REST API.
  * A GIT Commit.
* Configure an existing Validation Profile.

Here is a Use Case Diagram of the HTTP Validator :
![HTTP Validator Use Case Diagram.png](./images/UCD-HTTP_Validator.png)

## HTTP Validator Engine

### Validation Profile Model

A Validation Profile is a single json file that embed all the needed rules to validate your HTTP request.

Here you can find the model of a ValidationProfile :
![ValidationProfileModel.png](./images/ValidationProfileModel.png)

### How to create your Validation Profile
<!-- Gather inspiration from FHIR specifications with :
- A table that comments the attributes,
- Process by steps, from the main tag to the inner one.-->

### Validation Profile example
Here you can find a valid JSON Validation Profile :
[[ValidationProfile.json](./validationProfiles/ValidationProfile.json)]

### Validation Profile JSON Schema
Before sending/uploading your Validation Profile json file, you can test it with its json schema.
The latest version is available through this link :
[[ValidationProfileSchema.json](./validationProfiles/ValidationProfileSchema.json)]

## Glossary
* Validation Profile
* Assertion
