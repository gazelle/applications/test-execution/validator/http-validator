package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;

import java.io.Serial;

public class RegexCheck implements Check {

    @Serial
    private static final long serialVersionUID = -8011820762964484730L;
    private String regex;

    public String getRegex() {
        return this.regex;
    }

    public RegexCheck setRegex(String regex) {
        this.regex = regex;
        return this;
    }

    @Override
    public boolean isValid() {
        return this.regex != null && !this.regex.isEmpty() && !this.regex.isBlank();
    }
}
