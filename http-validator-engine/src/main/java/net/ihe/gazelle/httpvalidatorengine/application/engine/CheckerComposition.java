package net.ihe.gazelle.httpvalidatorengine.application.engine;

import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ChecksComposition;
import net.ihe.gazelle.httpvalidatorengine.interlay.validationreport.HttpAssertionResultHandler;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.EngineMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckerComposition {

    private final Logger logger = LoggerFactory.getLogger(CheckerComposition.class);

    public void validate(ChecksComposition composition, CheckerReports checkerReports, HttpAssertionResultHandler handler) {
        Assertion assertion = checkerReports.getAssertion();
        switch (composition) {
            case ONE_OF -> validateOneOf(checkerReports, assertion, handler);
            case ANY_OF -> validateAnyOf();
            case ALL_OF -> validateAllOf();
            case NOT -> validateNot();
            default -> logger.error("Unknown checksComposition key word.");
        }
    }

    private void validateOneOf(CheckerReports checkerReports, Assertion assertion, HttpAssertionResultHandler handler) {
        if (checkerReports.getInfos().size() == 1) {
            handler.info(checkerReports.infosToString(), assertion);
        } else {
            String oneOfError = EngineMessage.ONE_OF_ERROR.getFormattedValue(assertion.getId(), checkerReports.getInfos().size()) + "\n" + checkerReports.errorsToString();
            handler.error(oneOfError, assertion);
        }
    }

    private void validateAnyOf() {
        throw new IllegalCallerException("anyOf Not supported yet");
    }

    private void validateAllOf() {
        throw new IllegalCallerException("allOf Not supported yet");
    }

    private void validateNot() {
        throw new IllegalCallerException("not Not supported yet");
    }
}
