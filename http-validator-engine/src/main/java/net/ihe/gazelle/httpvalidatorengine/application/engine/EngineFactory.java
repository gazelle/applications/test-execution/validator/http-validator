package net.ihe.gazelle.httpvalidatorengine.application.engine;

import net.ihe.gazelle.httpvalidatorengine.interlay.parser.HttpMessageParser;

public class EngineFactory {

    public ELEngine createELEngine() {
        return new ELEngine();
    }

    public CheckerComposition createComposition() {
        return new CheckerComposition();
    }

    public HttpMessageParser createParser() {
        return new HttpMessageParser();
    }
}
