package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;

@JsonbPropertyOrder({"type", "fixedValue"})
public class FixedValueCheckDTO extends AbstractCheckDTO {

    public static final String JSON_VALUE_TYPE = "FIXEDVALUE";

    public FixedValueCheckDTO() {
        super(new FixedValueCheck());
    }

    @JsonbProperty(value = "fixedValue")
    public String getFixedValue() {
        return getDomainObject().getFixedValue();
    }

    public FixedValueCheckDTO setFixedValue(String fixedValue) {
        getDomainObject().setFixedValue(fixedValue);
        return this;
    }

    @JsonbTransient
    public FixedValueCheck getDomainObject() {
        return (FixedValueCheck) super.getCheck();
    }

    public FixedValueCheckDTO setDomainObject(FixedValueCheck domain) {
        super.setCheck(domain);
        return this;
    }
}
