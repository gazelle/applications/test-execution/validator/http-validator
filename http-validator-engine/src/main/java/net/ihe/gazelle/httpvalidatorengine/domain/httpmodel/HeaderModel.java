package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.ArrayList;
import java.util.List;

public class HeaderModel {

    private String name;
    private List<HeaderValue> values;

    public HeaderModel() {
    }

    public HeaderModel(String name, List<HeaderValue> values) {
        this.name = name;
        this.values = new ArrayList<>(values);
    }

    public String getName() {
        return name;
    }

    public HeaderModel setName(String name) {
        this.name = name;
        return this;
    }

    public List<HeaderValue> getValues() {
        return new ArrayList<>(values);
    }

    public HeaderModel setValues(List<HeaderValue> values) {
        this.values = new ArrayList<>(values);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name).append(": ");
        for (int i = 0; i < values.size(); i++) {
            stringBuilder.append(values.get(i).toString());
            if (i != values.size() - 1) {
                stringBuilder.append(",");
            }
        }
        return stringBuilder.toString();
    }

    public List<HeaderValue> filterHeaderValuesByParamName(String parameterName) {
        List<HeaderValue> headerValues = new ArrayList<>();
        for (HeaderValue headerValue : values) {
            if (headerValue.getParameterList().getParameters().stream().anyMatch(partName -> partName.getName().equals(parameterName))) {
                headerValues.add(headerValue);
            }
        }
        return headerValues;
    }

    public ParameterList parameters(String parameterName) {
        return new ParameterList(values.stream()
                .flatMap(headerValue -> headerValue.parameters(parameterName).getParameters().stream())
                .toList());
    }

}
