package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.ArrayList;
import java.util.List;

public abstract class Message {

    private String version;
    private List<HeaderModel> headers;

    public String getVersion() {
        return version;
    }

    protected Message setVersion(String version) {
        this.version = version;
        return this;
    }

    public List<HeaderModel> getMessageHeaders() {
        return new ArrayList<>(headers);
    }

    protected Message setMessageHeaders(List<HeaderModel> headers) {
        this.headers = new ArrayList<>(headers);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (HeaderModel headerModel : headers) {
            stringBuilder.append(headerModel.toString()).append("\r\n");
        }
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    public HeaderModel headers(String headerName) {
        List<HeaderModel> headerModels = headers.stream().filter(header -> header.getName().equals(headerName)).toList();
        if (headerModels.isEmpty()) {
            return new HeaderModel(headerName, new ArrayList<>());
        }
        return headerModels.get(0);
    }
}
