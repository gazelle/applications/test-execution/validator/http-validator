package net.ihe.gazelle.httpvalidatorengine.interlay.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils () {}

    public static String readFileFromName(String fileName) {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
        if (is == null) {
            throw new IllegalStateException(String.format("readFileFromName(%s) : Unable to find file.", fileName));
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("readFileFromName(%s) : Unable to find file.", fileName));
        }
    }

    public static Properties loadPropertiesFromResource(String fileName) {
        Properties messageProp = new Properties();
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
        try {
            messageProp.load(is);
        } catch (Exception e) {
            LOGGER.error(String.format("loadPropertiesFromResource(%s) : Unable to read properties.", fileName));
        }
        return messageProp;
    }

    public static Map<String, String> extractJsonStringFromZip(InputStream is) {
        Map<String, String> fileContents = new HashMap<>();
        if (is == null) {
            throw new IllegalStateException("extractJsonStringFromZip(InputStream) : Unable to extract ZIP.");
        }
        ZipInputStream zipInputStream = new ZipInputStream(is);
        try {
            ZipEntry entry = zipInputStream.getNextEntry();
            while (entry != null) {
                if (!entry.isDirectory()) {
                    fileContents.put(entry.getName(), readJsonFromStream(zipInputStream));
                }
                zipInputStream.closeEntry();
                entry = zipInputStream.getNextEntry();
            }
        } catch (IOException e) {
            LOGGER.error("extractJsonStringFromZip(InputStream) : Unable to extract ZIP.");
            throw new IllegalStateException(e.getCause());
        }
        return fileContents;
    }

    public static String readJsonFromStream(InputStream inputStream) throws IOException {
        StringBuilder jsonString = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        String line;
        while ((line = reader.readLine()) != null) {
            jsonString.append(line);
        }
        return jsonString.toString();
    }
}
