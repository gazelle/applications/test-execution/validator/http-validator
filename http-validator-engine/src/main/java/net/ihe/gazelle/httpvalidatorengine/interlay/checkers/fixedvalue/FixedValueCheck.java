package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;

import java.io.Serial;

public class FixedValueCheck implements Check {

    @Serial
    private static final long serialVersionUID = -605832652148337931L;
    private String fixedValue;

    public String getFixedValue() {
        return this.fixedValue;
    }

    public FixedValueCheck setFixedValue(String fixedValue) {
        this.fixedValue = fixedValue;
        return this;
    }

    @Override
    public boolean isValid() {
        return this.fixedValue != null && !this.fixedValue.isEmpty() && !this.fixedValue.isBlank();
    }
}
