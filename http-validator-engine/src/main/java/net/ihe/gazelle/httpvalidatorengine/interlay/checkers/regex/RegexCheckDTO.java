package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;

@JsonbPropertyOrder({"type", "regex"})
public class RegexCheckDTO extends AbstractCheckDTO {

    public static final String JSON_VALUE_TYPE = "REGEX";

    public RegexCheckDTO() {
        super(new RegexCheck());
    }

    @JsonbProperty(value = "regex")
    public String getRegex() {
        return getDomainObject().getRegex();
    }

    public RegexCheckDTO setRegex(String regex) {
        getDomainObject().setRegex(regex);
        return this;
    }

    @JsonbTransient
    public RegexCheck getDomainObject() {
        return (RegexCheck) super.getCheck();
    }

    public RegexCheckDTO setDomainObject(RegexCheck domain) {
        super.setCheck(domain);
        return this;
    }
}
