package net.ihe.gazelle.httpvalidatorengine.application.engine;

import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;

import java.util.List;

public interface Checker {

    void validate(List<String> contentList, Check check, CheckerReports checkerReports);
}
