package net.ihe.gazelle.httpvalidatorengine.interlay.dto;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbException;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.DTOFactory;

public class ValidationProfileAdapterImpl implements ValidationProfileAdapter {

    private final DTOFactory dtoFactory;

    public ValidationProfileAdapterImpl(DTOFactory dtoFactory) {
        this.dtoFactory = dtoFactory;
    }

    @Override
    public String adaptToString(ValidationProfile validationProfile) {
        try (Jsonb jsonb = JsonbBuilder.create()) {
            return jsonb.toJson(dtoFactory.createValidationProfileDTO().setDomainObject(validationProfile));
        } catch (JsonbException e) {
            throw new JsonbException(e.getMessage(), e.getCause());
        } catch (Exception e) {
            throw new ValidationProfileException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public ValidationProfile adaptFromString(String jsonString) {
        try (Jsonb jsonb = JsonbBuilder.create()) {
            return jsonb.fromJson(jsonString, ValidationProfileDTO.class).getDomainObject();
        } catch (JsonbException e) {
            throw new JsonbException(e.getMessage(), e.getCause());
        } catch (Exception e) {
            throw new ValidationProfileException(e.getMessage(), e.getCause());
        }
    }
}
