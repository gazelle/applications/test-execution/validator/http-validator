package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

public class ClosedListCheck implements Check {

    @Serial
    private static final long serialVersionUID = 2516556911383447723L;
    private List<String> values;

    public List<String> getValues() {
        return new ArrayList<>(values);
    }

    public ClosedListCheck setValues(List<String> values) {
        this.values = new ArrayList<>(values);
        return this;
    }

    @Override
    public boolean isValid() {
        return this.values != null && !this.values.isEmpty();
    }
}
