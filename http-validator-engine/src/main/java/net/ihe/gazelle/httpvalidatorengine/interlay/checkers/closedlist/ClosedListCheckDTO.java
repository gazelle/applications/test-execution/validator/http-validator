package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.AbstractCheckDTO;

import java.util.List;

@JsonbPropertyOrder({"type", "values"})
public class ClosedListCheckDTO extends AbstractCheckDTO {

    public static final String JSON_VALUE_TYPE = "CLOSEDLIST";

    public ClosedListCheckDTO() {
        super(new ClosedListCheck());
    }

    @JsonbProperty(value = "values")
    public List<String> getValues() {
        return getDomainObject().getValues();
    }

    public ClosedListCheckDTO setValues(List<String> values) {
        getDomainObject().setValues(values);
        return this;
    }

    @JsonbTransient
    public ClosedListCheck getDomainObject() {
        return (ClosedListCheck) super.getCheck();
    }

    public ClosedListCheckDTO setDomainObject(ClosedListCheck domain) {
        super.setCheck(domain);
        return this;
    }
}
