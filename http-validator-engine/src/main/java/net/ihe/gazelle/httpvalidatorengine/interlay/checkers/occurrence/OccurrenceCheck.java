package net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;

import java.io.Serial;

public class OccurrenceCheck implements Check {

    @Serial
    private static final long serialVersionUID = -8787119899009413336L;
    private Integer minOccurrence;
    private Integer maxOccurrence;

    public int getMinOccurrence() {
        return minOccurrence;
    }

    public OccurrenceCheck setMinOccurrence(int minOccurrence) {
        this.minOccurrence = minOccurrence;
        return this;
    }

    public int getMaxOccurrence() {
        return maxOccurrence;
    }

    public OccurrenceCheck setMaxOccurrence(int maxOccurrence) {
        this.maxOccurrence = maxOccurrence;
        return this;
    }

    @Override
    public boolean isValid() {
        return this.minOccurrence != null &&
               this.maxOccurrence != null &&
               this.minOccurrence <= this.maxOccurrence;
    }
}
