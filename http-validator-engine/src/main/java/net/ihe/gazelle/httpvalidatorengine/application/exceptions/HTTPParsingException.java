package net.ihe.gazelle.httpvalidatorengine.application.exceptions;

public class HTTPParsingException extends RuntimeException {

    public HTTPParsingException(String message) {
        super(message);
    }

    public HTTPParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
