package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

import java.util.List;

public class Request extends Message {

    private String method;
    private URIModel uri;

    public String getMethod() {
        return method;
    }

    public Request setMethod(String method) {
        this.method = method;
        return this;
    }

    public URIModel getUri() {
        return uri;
    }

    public Request setUri(URIModel uri) {
        this.uri = uri;
        return this;
    }

    @Override
    public Request setVersion(String version) {
        return (Request) super.setVersion(version);
    }

    @Override
    public Request setMessageHeaders(List<HeaderModel> headers) {
        return (Request) super.setMessageHeaders(headers);
    }

    @Override
    public String toString() {
        return this.method + " " + this.uri.toString() + " " + super.getVersion() + "\r\n" + super.toString();
    }
}
