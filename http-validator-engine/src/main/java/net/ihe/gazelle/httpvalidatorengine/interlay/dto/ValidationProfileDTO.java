package net.ihe.gazelle.httpvalidatorengine.interlay.dto;

import jakarta.json.bind.annotation.*;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.DTOFactory;

import java.util.List;

@JsonbPropertyOrder({"profileType", "id", "name", "description", "context", "assertions"})
public class ValidationProfileDTO {

    private ValidationProfile validationProfile;
    private DTOFactory dtoFactory;

    public ValidationProfileDTO() {
        this.validationProfile = new ValidationProfile();
    }

    @JsonbProperty(value = "profileType")
    public ProfileType getProfileType() {
        return this.validationProfile.getProfileType();
    }

    public ValidationProfileDTO setProfileType(ProfileType profileType) {
        this.validationProfile.setProfileType(profileType);
        return this;
    }

    @JsonbProperty(value = "id")
    public String getId() {
        return this.validationProfile.getId();
    }

    public ValidationProfileDTO setId(String id) {
        this.validationProfile.setId(id);
        return this;
    }

    @JsonbProperty(value = "name")
    public String getName() {
        return this.validationProfile.getName();
    }

    public ValidationProfileDTO setName(String name) {
        this.validationProfile.setName(name);
        return this;
    }

    @JsonbProperty(value = "description")
    public String getDescription() {
        return this.validationProfile.getDescription();
    }

    public ValidationProfileDTO setDescription(String description) {
        this.validationProfile.setDescription(description);
        return this;
    }

    @JsonbProperty(value = "context")
    public String getContext() {
        return this.validationProfile.getContext();
    }

    public ValidationProfileDTO setContext(String context) {
        this.validationProfile.setContext(context);
        return this;
    }

    @JsonbProperty("assertions")
    public List<AssertionDTO> getAssertions() {
        return this.validationProfile.getAssertions().stream().map(
                this::newAssertionDTO
        ).toList();
    }

    private AssertionDTO newAssertionDTO(Assertion assertion) {
        return dtoFactory.createAssertionDTO().setDomainObject(assertion);
    }

    public ValidationProfileDTO setAssertions(List<AssertionDTO> assertions) {
        this.validationProfile.setAssertions(assertions.stream().map(
                AssertionDTO::getDomainObject
        ).toList());
        return this;
    }

    @JsonbTransient
    public ValidationProfile getDomainObject() {
        return this.validationProfile;
    }

    public ValidationProfileDTO setDomainObject(ValidationProfile domain) {
        this.validationProfile = domain;
        return this;
    }

    public ValidationProfileDTO setDTOFactory(DTOFactory dtoFactory) {
        this.dtoFactory = dtoFactory;
        return this;
    }

    @JsonbTransient
    public boolean isValid() {
        return validationProfile.isValid();
    }
}
