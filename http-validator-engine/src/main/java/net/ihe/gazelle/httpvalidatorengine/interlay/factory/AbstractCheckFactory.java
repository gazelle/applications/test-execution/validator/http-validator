package net.ihe.gazelle.httpvalidatorengine.interlay.factory;

import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class AbstractCheckFactory<T> {

    protected Map<Class<?>, Class<? extends T>> init(Class<T> clazz) {
        Map<Class<?>, Class<? extends T>> cache = new HashMap<>();
        Reflections reflections = new Reflections("net.ihe.gazelle.httpvalidatorengine");
        Set<Class<? extends T>> subClasses = reflections.getSubTypesOf(clazz);
        for (Class<? extends T> subClass : subClasses) {
            findCheckType(cache, subClass);
        }
        return cache;
    }

    protected abstract void findCheckType(Map<Class<?>, Class<? extends T>> cache, Class<? extends T> subClass);

    protected T getInstance(Map<Class<?>, Class<? extends T>> cache, Object object) {
        try {
            return findMatchingClass(cache, object).getDeclaredConstructor().newInstance();
        } catch (NoSuchMethodException | IllegalAccessException |
                 InvocationTargetException | InstantiationException e) {
            throw new IllegalStateException("Unknown constructor");
        }
    }

    private Class<? extends T> findMatchingClass(Map<Class<?>, Class<? extends T>> cache, Object object) {
        if (cache.containsKey(object.getClass())) {
            return cache.get(object.getClass());
        } else {
            throw new IllegalStateException("Unknown " + cache.values().iterator().next().getSimpleName() + " Class.");
        }
    }
}
