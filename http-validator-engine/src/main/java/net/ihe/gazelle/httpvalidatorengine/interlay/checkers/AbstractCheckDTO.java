package net.ihe.gazelle.httpvalidatorengine.interlay.checkers;

import jakarta.json.bind.annotation.*;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Check;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist.ClosedListCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence.OccurrenceCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex.RegexCheckDTO;

@JsonbTypeInfo(key="type", value= {
        @JsonbSubtype(alias=FixedValueCheckDTO.JSON_VALUE_TYPE, type=FixedValueCheckDTO.class),
        @JsonbSubtype(alias=RegexCheckDTO.JSON_VALUE_TYPE, type=RegexCheckDTO.class),
        @JsonbSubtype(alias=OccurrenceCheckDTO.JSON_VALUE_TYPE, type=OccurrenceCheckDTO.class),
        @JsonbSubtype(alias=ClosedListCheckDTO.JSON_VALUE_TYPE, type=ClosedListCheckDTO.class)
})
public abstract class AbstractCheckDTO {

    private Check check;

    protected AbstractCheckDTO(Check check) {
        setCheck(check);
    }

    @JsonbTransient
    public Check getCheck() {
        return this.check;
    }

    public AbstractCheckDTO setCheck(Check domain) {
        this.check = domain;
        return this;
    }

    @JsonbTransient
    public boolean isValid() {
        return check.isValid();
    }
}
