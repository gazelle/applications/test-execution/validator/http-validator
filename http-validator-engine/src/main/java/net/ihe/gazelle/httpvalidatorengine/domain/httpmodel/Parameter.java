package net.ihe.gazelle.httpvalidatorengine.domain.httpmodel;

public class Parameter {

    private String name;
    private String value;

    public Parameter() {
    }

    public Parameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Parameter setName(String name) {
        this.name = name;
        return this;
    }

    public String getValue() {
        return value;
    }

    public Parameter setValue(String value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return name + "=" + value;
    }
}
