package mocks;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist.ClosedListCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.closedlist.ClosedListCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence.OccurrenceCheck;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.occurrence.OccurrenceCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex.RegexCheck;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.AssertionDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.fixedvalue.FixedValueCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.checkers.regex.RegexCheckDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapterImpl;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.CheckDTOFactory;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.CheckerFactory;
import net.ihe.gazelle.httpvalidatorengine.interlay.factory.DTOFactory;
import net.ihe.gazelle.httpvalidatorengine.interlay.profilevalidator.ValidationProfileValidatorImpl;

public class MockDTO {

    public static final CheckDTOFactory checkDTOFactory = new CheckDTOFactory();
    public static final DTOFactory dtoFactory = new DTOFactory(checkDTOFactory);
    public static final CheckerFactory checkerFactory = new CheckerFactory();

    public static FixedValueCheckDTO createFixedValueCheckerDTO(FixedValueCheck fixedValueCheck) {
        return new FixedValueCheckDTO().setDomainObject(fixedValueCheck);
    }

    public static RegexCheckDTO createRegexCheckerDTO(RegexCheck regexCheck) {
        return new RegexCheckDTO().setDomainObject(regexCheck);
    }

    public static OccurrenceCheckDTO createOccurrenceCheckerDTO(OccurrenceCheck occurrenceCheck) {
        return new OccurrenceCheckDTO().setDomainObject(occurrenceCheck);
    }

    public static ClosedListCheckDTO createClosedListCheckerDTO(ClosedListCheck closedListCheck) {
        return new ClosedListCheckDTO().setDomainObject(closedListCheck);
    }

    public static AssertionDTO createAssertionDTO(Assertion assertion) {
        return new AssertionDTO().setDomainObject(assertion).setCheckDTOFactory(checkDTOFactory);
    }

    public static ValidationProfileDTO createValidationProfileDTO(ValidationProfile validationProfile) {
        return new ValidationProfileDTO().setDomainObject(validationProfile).setDTOFactory(dtoFactory);
    }

    public static ValidationProfileAdapter createAdapterImpl() {
        return new ValidationProfileAdapterImpl(dtoFactory);
    }

    public static ValidationProfileValidatorImpl createValidationProfileValidatorImpl() {
        return new ValidationProfileValidatorImpl(createAdapterImpl());
    }
}
