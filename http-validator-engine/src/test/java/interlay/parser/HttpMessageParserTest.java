package interlay.parser;

import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Message;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Request;
import net.ihe.gazelle.httpvalidatorengine.domain.httpmodel.Response;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.HTTPParsingException;
import net.ihe.gazelle.httpvalidatorengine.interlay.parser.HttpMessageParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HttpMessageParserTest {

    private final HttpMessageParser messageParser = new HttpMessageParser();
    private static final String LINE_SEPARATOR = "\r\n";

    @Test
    void testRequest() {
        String httpMessage = "GET https://drim.ihe-catalyst.net/dcm4chee-arc/aets/DCM4CHEE/rs/studies/1.3.6.1.4.1.21367.2011.2.1.395.268.217/series/1.2.208.154.1.35676.16754.6008.16895.41500.25174.8989.57145/instances/1.2.208.154.1.7590.24235.34158.19355.34399.9894.30769.39102 HTTP/1.1" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**," +
                        "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**," +
                        "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR + LINE_SEPARATOR;

        Request message = (Request) messageParser.parse(httpMessage, ProfileType.HTTPREQUEST);

        assertEquals("https", message.getUri().getScheme());
    }

    @Test
    void testMultiHeaderName() {
        String httpMessageComma = "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**," +
                "transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR + LINE_SEPARATOR;
        String httpMessageMulti =  "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR + LINE_SEPARATOR;

        Message messageComma = messageParser.parse(httpMessageComma, ProfileType.HTTPREQUEST);
        Message messageMulti = messageParser.parse(httpMessageMulti, ProfileType.HTTPREQUEST);

        assertEquals(messageComma.headers("Accept").parameters("type").getParameters().get(0).getValue(),
                    messageMulti.headers("Accept").parameters("type").getParameters().get(0).getValue());
    }

    @Test
    void testResponse() {
        String httpMessage =  "HTTP/1.1 200 OK" + LINE_SEPARATOR +
                "Content-Type: application/dicom" + LINE_SEPARATOR +
                LINE_SEPARATOR + LINE_SEPARATOR;

        Response message = (Response) messageParser.parse(httpMessage, ProfileType.HTTPRESPONSE);

        assertEquals("OK", message.getStatusMessage());
    }

    @Test
    void testRequestKO() {
        String httpMessage = "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + "\r" +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR;

        assertThrows(HTTPParsingException.class, () -> messageParser.parse(httpMessage, ProfileType.HTTPREQUEST));
    }

    @Test
    void testRequestKO2() {
        String httpMessage = "GET /wado/rs?id=WIAJean HTTP/1.1" + LINE_SEPARATOR +
                "Host central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.9;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.8;boundary=**," +
                "multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.5;q=0.6;boundary=**" +
                LINE_SEPARATOR + LINE_SEPARATOR;

        assertThrows(HTTPParsingException.class, () -> messageParser.parse(httpMessage, ProfileType.HTTPREQUEST));
    }
}
