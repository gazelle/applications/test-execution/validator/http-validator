package application.engine;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import mocks.MockDTO;
import net.ihe.gazelle.httpvalidatorengine.application.engine.EngineFactory;
import net.ihe.gazelle.httpvalidatorengine.application.engine.HTTPValidatorEngine;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EngineTest {

    private static final String LINE_SEPARATOR = "\r\n";
    private static ValidationProfile profile;
    private static HTTPValidatorEngine engine;

    @BeforeAll
    static void setup() {
        String profileJson = FileUtils.readFileFromName("ok/ValidationProfile.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        profile = jsonb.fromJson(profileJson, ValidationProfileDTO.class).getDomainObject();
        engine = new HTTPValidatorEngine(new EngineFactory(), MockDTO.checkerFactory);
    }

    @Test
    void okEngineTest() {
        String httpMessage =  "GET /wado/rs HTTP/1.1" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: application/dicom " + LINE_SEPARATOR +
                LINE_SEPARATOR;

        ValidationSubReport report = engine.validate(httpMessage, profile);
        assertEquals(profile.getAssertions().size(), report.getAssertionReports().size());
        assertEquals(0, report.getSubCounters().getNumberOfFailedWithErrors());
    }

    @Test
    void koMethodEngineTest() {
        String httpMessage =  "POST wado/rs HTTP/1.0" + LINE_SEPARATOR +
                "Host: central-archive.ihe-europe.net" + LINE_SEPARATOR +
                "Accept: application/dicom " + LINE_SEPARATOR +
                LINE_SEPARATOR;

        ValidationSubReport report = engine.validate(httpMessage, profile);
        assertEquals(profile.getAssertions().size()-1, report.getAssertionReports().size());
        //assertEquals(2, report.getSubCounters().getNumberOfFailedWithErrors());
    }
}
