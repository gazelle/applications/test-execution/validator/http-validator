package application.engine;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import mocks.MockDTO;
import net.ihe.gazelle.httpvalidatorengine.application.engine.EngineFactory;
import net.ihe.gazelle.httpvalidatorengine.application.engine.HTTPValidatorEngine;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileDTO;
import net.ihe.gazelle.httpvalidatorengine.interlay.utils.FileUtils;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DebugEngineTest {

    private static final String LINE_SEPARATOR = "\r\n";
    private static HTTPValidatorEngine engine;

    @BeforeAll
    static void setup() {
        engine = new HTTPValidatorEngine(new EngineFactory(), MockDTO.checkerFactory);
    }

    @Test
    void debugEngineTest() {
        String httpMessage =  "GET /wado/rs HTTP/1.1" + LINE_SEPARATOR +
                "Host: debug" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.50;q=0.9;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.51;q=0.9;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.70;q=0.6;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.1;q=0.4;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.100;q=0.5;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.101;q=0.5;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.91;q=0.5;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.102;q=0.5;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.10;q=0.5;boundary=myboundary" + LINE_SEPARATOR +
                "Accept: multipart/related; type=\"application/dicom\";transfer-syntax=1.2.840.10008.1.2.4.80;q=0.5;boundary=myboundary" + LINE_SEPARATOR +
                LINE_SEPARATOR;

        String profileJson = FileUtils.readFileFromName("debugSamples/debugProfile1.json");
        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        ValidationProfile profile = jsonb.fromJson(profileJson, ValidationProfileDTO.class).getDomainObject();
        ValidationSubReport report = engine.validate(httpMessage, profile);
        assertEquals(0, report.getSubCounters().getNumberOfFailedWithErrors());
    }
}
