package domain.engine;

import mocks.MockDomainProfile;
import net.ihe.gazelle.httpvalidatorengine.domain.engine.CheckerReports;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckerReportTest {

    private static final MockDomainProfile MOCK = new MockDomainProfile();
    private static Assertion assertion;

    @BeforeAll
    static void setup() {
        assertion = MOCK.createHTTPVersionAssertion();
    }

    @Test
    void checkerInfoTest() {
        List<String> contentList = new ArrayList<>(Arrays.asList("GET", "POST"));
        CheckerReports checkerReports = new CheckerReports(contentList, assertion);
        assertEquals("GET", checkerReports.getContentList().get(0));
        assertEquals(assertion.getId(), checkerReports.getAssertion().getId());
        checkerReports.addInfo("true");
        checkerReports.addInfo("false");
        assertEquals(2, checkerReports.getInfos().size());
        assertEquals("true\nfalse", checkerReports.infosToString());
    }

    @Test
    void checkerErrorTest() {
        List<String> contentList = new ArrayList<>(Arrays.asList("GET", "POST"));
        CheckerReports checkerReports = new CheckerReports(contentList, assertion);
        checkerReports.addError("true");
        checkerReports.addError("false");
        assertEquals(2, checkerReports.getErrors().size());
        assertEquals("true\nfalse", checkerReports.errorsToString());
    }
}
