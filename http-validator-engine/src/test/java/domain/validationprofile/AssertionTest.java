package domain.validationprofile;

import mocks.MockDomainProfile;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.Assertion;
import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AssertionTest {

    MockDomainProfile domainMock = new MockDomainProfile();

    @Test
    void ok_httpMethodAssertionTest() {
        Assertion assertion = domainMock.createHTTPMethodAssertion_OK();
        assertTrue(assertion.isValid());
        assertEquals("request.method", assertion.getSelector());
        assertEquals("GETMethodChecking", assertion.getId());
        assertEquals("Assertion for the HTTP method checking", assertion.getDescription());
        assertEquals(RequirementPriority.MANDATORY, assertion.getRequirementPriority());
        assertEquals("FixedValueCheck", assertion.getChecks().get(0).getClass().getSimpleName());
    }

    @Test
    void ko_httpMethodAssertionTest() {
        Assertion assertion = domainMock.createHTTPMethodAssertion_KO();
        assertFalse(assertion.isValid());
    }

    @Test
    void ok_httpVersionAssertionTest() {
        Assertion assertion = domainMock.createHTTPVersionAssertion();
        assertTrue(assertion.isValid());
        assertEquals("request.version", assertion.getSelector());
        assertEquals("FixedValueCheck", assertion.getChecks().get(0).getClass().getSimpleName());
    }

    @Test
    void ok_requestPathAssertionTest() {
        Assertion assertion = domainMock.createRequestPathAssertion();
        assertTrue(assertion.isValid());
        assertEquals("request.uri.path", assertion.getSelector());
        assertEquals("RegexCheck", assertion.getChecks().get(0).getClass().getSimpleName());
    }

}
