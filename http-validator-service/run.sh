#!/bin/bash
# Configure cas resources
if [ ! -d /opt/http-validator/validationProfiles ]
  then
    mkdir -p /opt/http-validator/validationProfiles
fi

if [ "${DEBUG_REMOTE_ENABLED:-false}" = "true" ]
  then
    export JAVA_OPTS="$JAVA_OPTS -Xrunjdwp:transport=dt_socket,address=${JBOSS_HTTP_REMOTE_DEBUG_PORT},server=y,suspend=n"
fi

if [ "${DEBUG_LOCAL_ENABLED:-false}" = "true" ]
  then
    sed -i -e 's#level name="WARN"#level name="DEBUG"#g' ${JBOSS_HOME}/standalone/configuration/standalone.xml
fi

if [ "${JBOSS_PORTS_OFFSET_ENABLED:-false}" = "true" ]
  then
    export JAVA_OPTS="$JAVA_OPTS -Djboss.socket.binding.port-offset=${JBOSS_PORTS_OFFSET} -Djboss.http.port=8080"
fi

${JBOSS_HOME}/bin/standalone.sh -b ${JBOSS_BIND:-0.0.0.0}