package net.ihe.gazelle.it;

import io.restassured.response.Response;
import net.ihe.gazelle.it.client.ProfileClient;
import net.ihe.gazelle.modelmarshaller.application.StructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.factory.JsonObjectMapperBuilder;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.JsonStructureMapper;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.ServiceDTO;
import net.ihe.gazelle.validation.api.domain.metadata.structure.ValidationInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidationProfileControllerIT {

    private static final Logger LOG = LoggerFactory.getLogger(ValidationProfileControllerIT.class.getCanonicalName());
    private static ProfileClient profileClient;

    @BeforeAll
    static void setup() {
        RestConfig.setGlobalITConfig();
        profileClient = new ProfileClient();
    }

    @Test
    void testPostGetDeleteProfile() {
        LOG.info("testPostGetDeleteProfile() postValid() :");
        Response response = profileClient.postValidProfile();
        LOG.info("post response status: " + response.statusCode());
        LOG.info("post response body: " + response.getBody().print());
        assertEquals(200, response.statusCode());

        LOG.info("testPostGetDeleteProfile() first get() :");
        response = profileClient.getProfiles();
        LOG.info("get response status: " + response.statusCode());
        LOG.info("get response body: " + response.getBody().print());
        assertEquals(200, response.statusCode());
        ValidationInterface validationInterface = extractProfileMetadata(response.getBody().print());
        assertEquals(1, validationInterface.getValidationProfiles().size());
        String id = validationInterface.getValidationProfiles().get(0).getProfileID();
        assertEquals("WADO-RS_Request_Validation_Profile", id);
        assertEquals("WADO_HTTPRequest", validationInterface.getValidationProfiles().get(0).getProfileName());

        LOG.info("testPostGetDeleteProfile() delete() :");
        response = profileClient.deleteProfile(id);
        LOG.info("delete response status: " + response.statusCode());
        LOG.info("delete response body: " + response.getBody().print());
        assertEquals(200, response.statusCode());

        LOG.info("testPostGetDeleteProfile() second get() :");
        response = profileClient.getProfiles();
        assertEquals(200, response.statusCode());
        validationInterface = extractProfileMetadata(response.getBody().print());
        assertEquals(0, validationInterface.getValidationProfiles().size());
    }

    @Test
    void testPostBatchProfile() {
        LOG.info("testPostBatchProfile() postBatch() :");
        Response response = profileClient.postProfileBatch();
        LOG.info("post response status: " + response.statusCode());
        LOG.info("post response body: " + response.getBody().print());
        assertEquals(200, response.statusCode());
        response = profileClient.getProfiles();
        assertEquals(200, response.statusCode());
        ValidationInterface validationInterface = extractProfileMetadata(response.getBody().print());
        assertEquals(2, validationInterface.getValidationProfiles().size());
        String id1 = validationInterface.getValidationProfiles().get(0).getProfileID();
        String id2 = validationInterface.getValidationProfiles().get(1).getProfileID();
        assertEquals("WADO-RS_Request_Validation_Profile2", id1);
        assertEquals("WADO_HTTPRequest", validationInterface.getValidationProfiles().get(0).getProfileName());
        assertEquals("WADO-RS_Request_Validation_Profile", id2);
        assertEquals("WADO_HTTPRequest", validationInterface.getValidationProfiles().get(1).getProfileName());
        profileClient.deleteProfile(id1);
        profileClient.deleteProfile(id2);
    }

    @Test
    void testPostAlreadyUsedError() {
        LOG.info("testPostAlreadyUsedError() postValid() :");
        Response response = profileClient.postValidProfile();
        assertEquals(200, response.statusCode());
        response = profileClient.postValidProfile();
        assertEquals(400, response.statusCode());
        assertTrue(response.getBody().print().contains("already used"));
        profileClient.deleteProfile("WADO-RS_Request_Validation_Profile");
    }

    @Test
    void testDeleteNotExistingError() {
        LOG.info("testDeleteNotExistingError() delete() :");
        Response response = profileClient.deleteProfile("WADO-RS_Request_Validation_Profile");
        assertEquals(404, response.statusCode());
        assertTrue(response.getBody().print().contains("Unable to find ValidationProfile"));
    }

    @Test
    void testPostWrongProfileError() {
        LOG.info("testPostWrongProfileError() postNotValid() :");
        Response response = profileClient.postNotValidProfile();
        assertEquals(400, response.statusCode());
        assertTrue(response.getBody().print().contains("profileType"));
    }

    private ValidationInterface extractProfileMetadata(String response) {
        StructureMapper structureFactory = new JsonStructureMapper(new JsonObjectMapperBuilder().build());
        ServiceDTO serviceDTO = structureFactory.unmarshall(response, ServiceDTO.class);
        return (ValidationInterface) serviceDTO.getProvidedInterfacesDTOs().get(0).getDomainObject();
    }
}