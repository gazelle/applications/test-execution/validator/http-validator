package net.ihe.gazelle.it.mock;

import org.mockserver.mock.Expectation;
import org.mockserver.server.initialize.ExpectationInitializer;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class GazelleTokenExpectationInitializer implements ExpectationInitializer {

    private static final String MOCKED_TOKEN = "{\"creationDate\":\"2023-02-17T12:48:16.276531\",\"expirationDate\":\"2040-02-17T12:48:16.276546\",\"organization\":\"KEREVAL\",\"roles\":[\"admin_role\"],\"username\":\"mock\",\"value\":\"mock_token\"}";

    @Override
    public Expectation[] initializeExpectations() {
        try{
            return new Expectation[]{
                    new Expectation(request()
                            .withPath("/health"))
                            .thenRespond(response()
                            .withStatusCode(200)
                    ),
                    new Expectation(request()
                            .withMethod("GET")
                            .withPath("/rest/v1/authn/user/mock_token"))
                            .thenRespond(response()
                            .withBody(MOCKED_TOKEN)
                            .withStatusCode(200)
                    ),
            };
        }
        catch (Exception e) {
            e.printStackTrace();
            return new Expectation[0];
        }
    }
}
