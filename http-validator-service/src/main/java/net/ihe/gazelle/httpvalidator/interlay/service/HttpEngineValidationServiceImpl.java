package net.ihe.gazelle.httpvalidator.interlay.service;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidator.application.service.HttpEngineValidationService;
import net.ihe.gazelle.httpvalidatorengine.application.engine.Engine;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.EngineException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.validation.api.domain.report.structure.AssertionReport;
import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;

import java.nio.charset.StandardCharsets;

@RequestScoped
public class HttpEngineValidationServiceImpl implements HttpEngineValidationService {

    @Inject
    Engine engine;

    public ValidationSubReport validate(byte[] contentByte, ValidationProfile validationProfile) {
        String content = new String(contentByte, StandardCharsets.UTF_8);
        try {
            return engine.validate(content, validationProfile);
        } catch (EngineException e) {
            return new ValidationSubReport()
                    .setName("Not Well Formed Selector")
                    .setStandards(new String[]{"HTTP"})
                    .addAssertionReport(new AssertionReport()
                            .setAssertionID("Not Well Formed Selector")
                            .setDescription(e.getMessage())
                            .setResult(ValidationTestResult.FAILED)
                            .setPriority(RequirementPriority.MANDATORY));
        }
    }
}
