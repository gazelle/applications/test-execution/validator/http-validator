package net.ihe.gazelle.httpvalidator.interlay.ws;

import jakarta.json.bind.JsonbException;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.httpvalidator.application.exceptions.ProfileNotFoundException;
import net.ihe.gazelle.httpvalidator.interlay.service.ServiceMessage;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    private ExceptionHandler() {
    }

    public static Response handleRestException(Exception e) {
        LOGGER.debug(e.getMessage());
        if (e instanceof JsonbException) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("ERROR : Unable to parse ValidationProfile.")
                    .build();
        } else if (e instanceof ValidationProfileException) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build();
        } else if (e instanceof ProfileNotFoundException) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(e.getMessage())
                    .build();
        } else {
            LOGGER.error(e.getMessage());
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ServiceMessage.INTERNAL_ERROR.getFormattedValue())
                    .build();
        }
    }
}
