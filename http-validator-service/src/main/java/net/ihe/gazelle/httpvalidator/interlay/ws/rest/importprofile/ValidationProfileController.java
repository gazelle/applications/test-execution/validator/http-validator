package net.ihe.gazelle.httpvalidator.interlay.ws.rest.importprofile;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.EntityPart;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;

@Path("validationprofiles")
@Produces(MediaType.APPLICATION_JSON)
public interface ValidationProfileController {

    @GET
    @Path("/id/{id}")
    Response getValidationProfile(@PathParam("id") String id);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response importValidationProfile(String json);

    @POST
    @Path("/batch")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    Response importValidationProfiles(List<EntityPart> parts);

    @DELETE
    @Path("/delete")
    Response deleteValidationProfile(@QueryParam("id") String id);
}
