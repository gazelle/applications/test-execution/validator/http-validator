package net.ihe.gazelle.httpvalidator.interlay.service;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidator.application.service.WellFormedValidationService;
import net.ihe.gazelle.httpvalidatorengine.application.engine.EngineFactory;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.HTTPParsingException;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ProfileType;
import net.ihe.gazelle.validation.api.domain.report.structure.AssertionReport;
import net.ihe.gazelle.validation.api.domain.report.structure.RequirementPriority;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;

import java.nio.charset.StandardCharsets;

@RequestScoped
public class WellFormedValidationServiceImpl implements WellFormedValidationService {

    @Inject
    EngineFactory engineFactory;
    private boolean isWellFormed = true;

    @Override
    public boolean isWellFormed() {
        return isWellFormed;
    }

    @Override
    public ValidationSubReport validate(byte[] contentByte, ProfileType profileType) {
        String content = new String(contentByte, StandardCharsets.UTF_8);
        try {
            engineFactory.createParser().parse(content, profileType);
            return new ValidationSubReport()
                    .setName("Well Formed Validation")
                    .setStandards(new String[]{"HTTP"})
                    .addAssertionReport(new AssertionReport()
                            .setAssertionID("Well Formed Validation")
                            .setDescription("Success: The document you have validated is supposed to be a well-formed document.")
                            .setResult(ValidationTestResult.PASSED)
                            .setPriority(RequirementPriority.MANDATORY));
        } catch (HTTPParsingException e) {
            isWellFormed = false;
            return new ValidationSubReport()
                    .setName("Not Well Formed Validation")
                    .setStandards(new String[]{"HTTP"})
                    .addAssertionReport(new AssertionReport()
                            .setAssertionID("Not Well Formed Validation")
                            .setDescription(e.getMessage())
                            .setResult(ValidationTestResult.FAILED)
                            .setPriority(RequirementPriority.MANDATORY));
        }
    }
}
