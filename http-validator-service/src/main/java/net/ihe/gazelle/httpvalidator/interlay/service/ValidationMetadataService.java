package net.ihe.gazelle.httpvalidator.interlay.service;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidator.interlay.config.ProfileConfiguration;
import net.ihe.gazelle.servicemetadata.api.application.builder.ServiceBuilder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.config.MetadataServiceProvider;
import net.ihe.gazelle.validation.api.domain.metadata.structure.ValidationInterface;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.List;

@RequestScoped
@ValidationMetadata
public class ValidationMetadataService extends MetadataServiceProvider {

    @Inject
    @ConfigProperty(name = "gzl.service.name")
    private String serviceName;
    @Inject
    @ConfigProperty(name = "gzl.service.version")
    private String serviceVersion;

    @Inject
    ValidationProfileService validationProfileService;

    private final ValidatorBuilderFactory validatorBuilderFactory = new BValidatorBuilderFactory();

    @Override
    public Service getMetadata() {
        return constructService(validationProfileService.getProfiles());
    }

    private Service constructService(List<ProfileMetadata> profileMetadataList) {
        ProfileConfiguration profileConfiguration = new ProfileConfiguration(profileMetadataList);
        String applicationUrl = this.validationProfileService.getPreferences().getApplicationUrl();
        ServiceBuilder serviceObjectBuilder = new ServiceBuilder(validatorBuilderFactory)
                .setName(this.serviceName)
                .setVersion(this.serviceVersion)
                .setInstanceId("NOT_SET")
                .setReplicaId("NOT_SET")
                .addProvidedInterface(new ValidationInterface()
                        .setInterfaceName("ValidationInterface")
                        .setInterfaceVersion("1.0.0")
                        .setRequired(true)
                        .setValidationProfiles(
                                profileConfiguration.getValidationProfiles()
                        )
                        .setBindings(List.of(new RestBinding().setServiceUrl(applicationUrl)))
                );
        return serviceObjectBuilder.build();
    }
}
