package net.ihe.gazelle.httpvalidator.interlay.dao;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Default;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.application.dao.ValidationProfileDAO;
import net.ihe.gazelle.httpvalidator.application.config.Preferences;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;

import java.nio.file.Path;
import java.util.List;

@Default
@ApplicationScoped
public class ValidationProfileDAOCacheImpl implements ValidationProfileDAO {

    private final Preferences preferences;
    @DirectDAO
    private ValidationProfileDAO validationProfileDAO;
    private LoadingCache<String, ValidationProfile> cache;

    @Inject
    public ValidationProfileDAOCacheImpl(Preferences preferences, @DirectDAO ValidationProfileDAO validationProfileDAO) {
        this.preferences = preferences;
        this.validationProfileDAO = validationProfileDAO;
    }

    public void init() {
        cache = Caffeine.newBuilder()
                .maximumSize(preferences.getMaxCacheElements())
                .build(validationProfileDAO::getValidationProfileByID);
        getAllProfiles();
    }

    @Override
    public Preferences getPreferences() {
        return this.preferences;
    }

    @Override
    public List<ProfileMetadata> getProfileMetadata() {
        return validationProfileDAO.getProfileMetadata();
    }

    @Override
    public List<ValidationProfile> getAllProfiles() {
        return validationProfileDAO.getAllProfiles();
    }

    @Override
    public ValidationProfile getValidationProfileByID(String id) {
        ValidationProfile validationProfile = cache.get(id);
        if (validationProfile == null) {
            validationProfile = validationProfileDAO.getValidationProfileByID(id);
            cache.put(id, validationProfile);
        }
        return validationProfile;
    }

    @Override
    public Path getProfilePathById(String id) {
        return validationProfileDAO.getProfilePathById(id);
    }

    @Override
    public void saveValidationProfile(ValidationProfile validationProfile) {
        validationProfileDAO.saveValidationProfile(validationProfile);
        cache.put(validationProfile.getId(), validationProfile);
    }

    @Override
    public void deleteValidationProfile(String id) {
        validationProfileDAO.deleteValidationProfile(id);
        cache.invalidate(id);
    }
}
