package net.ihe.gazelle.httpvalidator.interlay.ws.rest.importprofile;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.core.EntityPart;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidator.interlay.service.ServiceMessage;
import net.ihe.gazelle.httpvalidator.interlay.ws.ExceptionHandler;
import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.httpvalidatorengine.interlay.dto.ValidationProfileAdapter;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class ValidationProfileControllerImpl implements ValidationProfileController {

    @Inject
    private ValidationProfileService service;
    @Inject
    private ValidationProfileAdapter adapter;

    @Override
    public Response getValidationProfile(String id) {
        try {
            ValidationProfile validationProfile = service.getAvailableProfile(id);
            return Response.status(Response.Status.OK).entity(adapter.adaptToString(validationProfile)).build();
        } catch (Exception e) {
            return ExceptionHandler.handleRestException(e);
        }
    }

    @Override
    public Response importValidationProfile(String json) {
        try {
            ValidationProfile profile = service.validateProfile(json);
            service.importProfile(profile);
            return Response.status(Response.Status.OK)
                    .entity(ServiceMessage.VALID_PROFILE.getFormattedValue(profile.getId())).build();
        } catch (Exception e) {
            return ExceptionHandler.handleRestException(e);
        }
    }

    @Override
    public Response importValidationProfiles(List<EntityPart> parts) {
        List<String> batchResult = new ArrayList<>();
        try (Jsonb jsonb = JsonbBuilder.create()) {
            for (EntityPart part : parts) {
                InputStream is = part.getContent();
                batchResult.addAll(service.importProfileBatch(is));
            }
            return Response.status(Response.Status.OK).entity(jsonb.toJson(batchResult)).build();
        } catch (Exception e) {
            return ExceptionHandler.handleRestException(e);
        }
    }

    @Override
    public Response deleteValidationProfile(String id) {
        try {
            service.deleteProfile(id);
            return Response.status(Response.Status.OK)
                    .entity(ServiceMessage.DELETE_SUCCESSFUL.getFormattedValue(id)).build();
        } catch (Exception e) {
            return ExceptionHandler.handleRestException(e);
        }
    }
}
