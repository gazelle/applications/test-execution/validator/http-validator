package net.ihe.gazelle.httpvalidator.application.service;

import net.ihe.gazelle.httpvalidatorengine.domain.validationprofile.ValidationProfile;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;

public interface HttpEngineValidationService {
    ValidationSubReport validate(byte[] contentByte, ValidationProfile validationProfile);
}
