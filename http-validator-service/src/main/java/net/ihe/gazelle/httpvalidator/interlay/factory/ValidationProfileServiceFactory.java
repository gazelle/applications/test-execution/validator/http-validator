package net.ihe.gazelle.httpvalidator.interlay.factory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import net.ihe.gazelle.httpvalidator.application.dao.ValidationProfileDAO;
import net.ihe.gazelle.httpvalidator.application.service.ValidationProfileService;
import net.ihe.gazelle.httpvalidator.interlay.service.ValidationProfileServiceImpl;
import net.ihe.gazelle.httpvalidatorengine.application.profilevalidator.ValidationProfileValidator;

@ApplicationScoped
public class ValidationProfileServiceFactory {

    @Inject
    ValidationProfileDAO validationProfileDAO;
    @Inject
    ValidationProfileValidator validationProfileValidator;

    @Produces
    public ValidationProfileService createService() {
        return new ValidationProfileServiceImpl(validationProfileDAO, validationProfileValidator);
    }
}
