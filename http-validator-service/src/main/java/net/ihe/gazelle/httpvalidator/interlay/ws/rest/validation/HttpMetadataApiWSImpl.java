package net.ihe.gazelle.httpvalidator.interlay.ws.rest.validation;

import jakarta.inject.Inject;
import jakarta.ws.rs.Path;
import net.ihe.gazelle.httpvalidator.interlay.service.ValidationMetadata;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.api.application.MetadataServiceFactory;
import net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay.MetadataApiWSImpl;

@Path("/metadata")
public class HttpMetadataApiWSImpl extends MetadataApiWSImpl {

    @Inject
    @ValidationMetadata
    MetadataService validationMetadataService;

    @Override
    protected MetadataServiceFactory getMetadataServiceFactory() {
        return () -> validationMetadataService;
    }
}
