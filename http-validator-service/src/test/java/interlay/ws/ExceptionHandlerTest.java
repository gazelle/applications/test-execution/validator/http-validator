package interlay.ws;

import jakarta.json.bind.JsonbException;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.httpvalidator.application.exceptions.ProfileNotFoundException;
import net.ihe.gazelle.httpvalidator.interlay.ws.ExceptionHandler;
import net.ihe.gazelle.httpvalidatorengine.application.exceptions.ValidationProfileException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExceptionHandlerTest {

    @Test
    void jsonbExceptionTest() {
        JsonbException e = new JsonbException("any message");
        Response response = ExceptionHandler.handleRestException(e);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    void validationProfileExceptionTest() {
        ValidationProfileException e = new ValidationProfileException("any message");
        Response response = ExceptionHandler.handleRestException(e);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    void profileNotFoundExceptionTest() {
        ProfileNotFoundException e = new ProfileNotFoundException("any message");
        Response response = ExceptionHandler.handleRestException(e);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    void defaultExceptionTest() {
        Exception e = new Exception("any message");
        Response response = ExceptionHandler.handleRestException(e);
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
    }

}
