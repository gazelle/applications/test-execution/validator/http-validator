package interlay.config;

import net.ihe.gazelle.httpvalidator.application.dao.ProfileMetadata;
import net.ihe.gazelle.httpvalidator.interlay.config.ProfileConfiguration;
import net.ihe.gazelle.validation.api.domain.metadata.structure.ValidationProfile;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProfileConfigurationTest {

    @Test
    void configurationTest() {
        ProfileMetadata profileMetadata = new ProfileMetadata("HTTPREQUEST", "id", "name", "description", "context", "path");
        List<ProfileMetadata> profileMetadataList = List.of(profileMetadata);
        ProfileConfiguration profileConfiguration = new ProfileConfiguration(profileMetadataList);
        ValidationProfile apiValidationProfile = profileConfiguration.getValidationProfiles().get(0);
        assertEquals("id", apiValidationProfile.getProfileID());
        assertEquals("name", apiValidationProfile.getProfileName());
        assertEquals("context", apiValidationProfile.getDomain());
    }
}
