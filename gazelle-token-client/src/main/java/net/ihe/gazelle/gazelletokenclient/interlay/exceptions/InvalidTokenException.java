package net.ihe.gazelle.gazelletokenclient.interlay.exceptions;

public class InvalidTokenException extends RuntimeException {
    public InvalidTokenException(String s) {
        super(s);
    }
}
