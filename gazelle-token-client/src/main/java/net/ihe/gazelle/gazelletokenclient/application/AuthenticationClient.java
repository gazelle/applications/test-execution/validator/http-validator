package net.ihe.gazelle.gazelletokenclient.application;

import net.ihe.gazelle.gazelletokenclient.domain.IdentityToken;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.ExpiredTokenException;
import net.ihe.gazelle.gazelletokenclient.interlay.exceptions.InvalidTokenException;

public interface AuthenticationClient {
    IdentityToken requestGazelleTokenService(String endpoint, String token) throws InvalidTokenException, ExpiredTokenException;
}
