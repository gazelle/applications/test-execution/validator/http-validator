<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>net.ihe.gazelle</groupId>
    <artifactId>http-validator</artifactId>
    <version>0.3.2-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>HTTP Validator</name>
    <url>https://gazelle.ihe.net</url>

    <modules>
        <module>gazelle-token-client</module>
        <module>http-validator-engine</module>
        <module>http-validator-service</module>
        <module>http-validator-editor</module>
    </modules>

    <scm>
        <connection>scm:git:${git.project.url}</connection>
        <developerConnection>scm:git:${git.project.url}</developerConnection>
        <url>${git.project.url}</url>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <java.version>17</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <git.user.name>gitlab-ci</git.user.name>
        <git.user.token>changeit</git.user.token>
        <git.project.url>https://${git.user.name}:${git.user.token}@gitlab.inria.fr/gazelle/public/validation/http-validator.git</git.project.url>
        <!-- Plugin Management dependencies -->
        <docker-maven-plugin.version>0.40.3</docker-maven-plugin.version>
        <!-- Plugin dependencies -->
        <maven-compiler-plugin.version>3.8.1</maven-compiler-plugin.version>
        <maven-surefire-plugin.version>3.0.0-M7</maven-surefire-plugin.version>
        <maven-dependency-plugin.version>2.8</maven-dependency-plugin.version>
        <maven-resources-plugin.version>3.0.2</maven-resources-plugin.version>
        <jacoco-maven-plugin.version>0.8.8</jacoco-maven-plugin.version>
        <maven-release-plugin.version>2.5.3</maven-release-plugin.version>
        <!-- Plugin dependencies in modules -->
        <build-helper-maven-plugin.version>3.3.0</build-helper-maven-plugin.version>
        <maven-failsafe-plugin.version>3.0.0-M7</maven-failsafe-plugin.version>
        <maven-war-plugin.version>3.3.1</maven-war-plugin.version>
        <maven-jar-plugin.version>3.3.0</maven-jar-plugin.version>
        <!-- Profile plugin versions -->
        <sonar-maven-plugin.version>3.9.1.2184</sonar-maven-plugin.version>
        <nexus-staging-maven-plugin.version>1.6.12</nexus-staging-maven-plugin.version>
        <maven-javadoc-plugin.version>3.1.1</maven-javadoc-plugin.version>
        <!-- Dependencies version -->
        <wildfly.version>27.0.0.Final</wildfly.version>
        <mockserver.version>5.15.0</mockserver.version>
        <slf4j-simple.version>2.0.5</slf4j-simple.version>
        <junit.version>4.13.2</junit.version>
        <junit-jupiter.version>5.9.2</junit-jupiter.version>
        <mockito.version>5.2.0</mockito.version>
        <!-- Integration Tests -->
        <docker.image.tag>${project.version}</docker.image.tag>
        <docker.registry.path>rg.fr-par.scw.cloud/gazelle-snapshot/app</docker.registry.path>
        <it.base.uri>http://localhost</it.base.uri>
        <it.base.port>8480</it.base.port>
        <it.service.name>http-validator</it.service.name>
        <it.service.version>${project.version}</it.service.version>
        <it.consumer.timeout>30000</it.consumer.timeout>
        <it.env.application.url>http://localhost:8480/http-validator</it.env.application.url>
        <it.env.token.service.url>http://http-server-mock:8082</it.env.token.service.url>
        <it.env.profileRepoPath>/opt/http-validator/validationProfiles</it.env.profileRepoPath>
        <it.env.cacheMaxElem>50</it.env.cacheMaxElem>

        <skipITs>true</skipITs>
        <skipUnitTests>false</skipUnitTests>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>http-validator-editor</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>http-validator-engine</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>http-validator-service</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>gazelle-token-client</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wildfly.bom</groupId>
                <artifactId>wildfly-ee</artifactId>
                <version>${wildfly.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Gazelle -->
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>validation-api</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>validation-jaxrs-ws</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>service-metadata-jaxrs-ws</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>rawhttp-core</artifactId>
            <version>1.0.0</version>
            <exclusions>
                <exclusion>
                    <groupId>*</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <!-- Wildfly (provided) dependencies -->
        <dependency>
            <groupId>jakarta.ws.rs</groupId>
            <artifactId>jakarta.ws.rs-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.json.bind</groupId>
            <artifactId>jakarta.json.bind-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse</groupId>
            <artifactId>yasson</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.servlet</groupId>
            <artifactId>jakarta.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.inject</groupId>
            <artifactId>jakarta.inject-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.ejb</groupId>
            <artifactId>jakarta.ejb-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.enterprise</groupId>
            <artifactId>jakarta.enterprise.cdi-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.interceptor</groupId>
            <artifactId>jakarta.interceptor-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jakarta.xml.bind</groupId>
            <artifactId>jakarta.xml.bind-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.resteasy</groupId>
            <artifactId>resteasy-jaxb-provider</artifactId>
            <scope>provided</scope>
        </dependency>
        <!-- Other dependencies -->
        <dependency>
            <groupId>com.networknt</groupId>
            <artifactId>json-schema-validator</artifactId>
            <version>1.0.77</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.microprofile.config</groupId>
            <artifactId>microprofile-config-api</artifactId>
            <version>3.0.3</version>
        </dependency>
        <!-- Logging dependencies -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>${slf4j-simple.version}</version>
        </dependency>
        <!-- Unit Test dependencies -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <id>IHE</id>
            <name>IHE Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>nexus-releases</id>
            <url>https://gazelle.ihe.net/nexus/content/repositories/releases</url>
        </repository>
        <snapshotRepository>
            <id>nexus-snapshots</id>
            <url>https://gazelle.ihe.net/nexus/content/repositories/snapshots/</url>
        </snapshotRepository>
    </distributionManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <!-- build http-validator Docker image at maven package phase, and start http-validator container for integration-tests -->
                    <!-- In section plugin-management, because only activated with profile dev -->
                    <groupId>io.fabric8</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>${docker-maven-plugin.version}</version>
                    <configuration>
                        <autoCreateCustomNetworks>true</autoCreateCustomNetworks>
                        <images>
                            <image>
                                <name>mockserver/mockserver:${mockserver.version}</name>
                                <alias>http-server</alias>
                                <run>
                                    <network>
                                        <mode>custom</mode>
                                        <name>localit</name>
                                        <alias>http-server-mock</alias>
                                    </network>
                                    <ports>
                                        <port>8082:8082</port>
                                    </ports>
                                    <env>
                                        <SERVER_PORT>8082</SERVER_PORT>
                                        <MOCKSERVER_PORT>8082</MOCKSERVER_PORT>
                                        <MOCKSERVER_INITIALIZATION_CLASS>net.ihe.gazelle.it.mock.GazelleTokenExpectationInitializer</MOCKSERVER_INITIALIZATION_CLASS>
                                    </env>
                                    <volumes>
                                        <bind>
                                            <volume>${project.basedir}/target/:/libs</volume>
                                        </bind>
                                    </volumes>
                                    <wait>
                                        <time>${it.consumer.timeout}</time>
                                        <http>
                                            <url>http://localhost:8082/health</url>
                                            <method>GET</method>
                                            <status>200</status>
                                        </http>
                                    </wait>
                                </run>
                            </image>
                            <image>
                                <name>${docker.registry.path}/http-validator:${docker.image.tag}</name>
                                <alias>${project.parent.artifactId}</alias>
                                <build>
                                    <dockerFile>${project.basedir}/Dockerfile</dockerFile>
                                </build>
                                <run>
                                    <network>
                                        <mode>custom</mode>
                                        <name>localit</name>
                                        <alias>${project.parent.artifactId}</alias>
                                    </network>
                                    <ports>
                                        <port>${it.base.port}:8080</port>
                                        <port>8787:8787</port>
                                    </ports>
                                    <env>
                                        <APPLICATION_URL>${it.env.application.url}</APPLICATION_URL>
                                        <GZL_TOKEN_API_URL>${it.env.token.service.url}</GZL_TOKEN_API_URL>
                                        <GZL_PROFILE_REPOSITORY_PATH>${it.env.profileRepoPath}</GZL_PROFILE_REPOSITORY_PATH>
                                        <GZL_PROFILE_CACHE_MAX_ELEMENTS>${it.env.cacheMaxElem}</GZL_PROFILE_CACHE_MAX_ELEMENTS>
                                        <JBOSS_BIND>0.0.0.0</JBOSS_BIND>
                                    </env>
                                    <dependsOn>
                                        <container>http-server</container>
                                    </dependsOn>
                                    <wait>
                                        <time>${it.consumer.timeout}</time>
                                        <http>
                                            <url>http://localhost:${it.base.port}/http-validator/index.html</url>
                                            <method>GET</method>
                                            <status>200</status>
                                        </http>
                                    </wait>
                                </run>
                            </image>
                        </images>
                    </configuration>
                    <executions>
                        <execution>
                            <id>buildImage</id>
                            <phase>package</phase>
                            <goals>
                                <goal>build</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>startHTTPValidator</id>
                            <phase>pre-integration-test</phase>
                            <goals>
                                <goal>start</goal>
                            </goals>
                            <configuration>
                                <skip>${skipITs}</skip>
                            </configuration>
                        </execution>
                        <execution>
                            <id>stopAndRemoveHTPPValidator</id>
                            <phase>post-integration-test</phase>
                            <goals>
                                <goal>stop</goal>
                            </goals>
                            <configuration>
                                <skip>${skipITs}</skip>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${maven-jar-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>JarTests</id>
                            <phase>package</phase>
                            <goals>
                                <goal>test-jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <compilerVersion>${java.version}</compilerVersion>
                    <fork>true</fork>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <skip>${skipUnitTests}</skip>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-engine</artifactId>
                        <version>${junit-jupiter.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>${maven-dependency-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>${maven-resources-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven-release-plugin.version}</version>
                <configuration>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <releaseProfiles>release</releaseProfiles>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>unit-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>unit-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>dev</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <profile>
            <id>sonar</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonarsource.scanner.maven</groupId>
                        <artifactId>sonar-maven-plugin</artifactId>
                        <version>${sonar-maven-plugin.version}</version>
                        <executions>
                            <execution>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sonar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>${nexus-staging-maven-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>default-deploy</id>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <serverId>nexus-releases</serverId>
                            <nexusUrl>https://gazelle.ihe.net/nexus</nexusUrl>
                            <skipStaging>true</skipStaging>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>